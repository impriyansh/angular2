import { baseURL } from './baseurl';

//function for setting default restangular configuration
export function RestangularConfigFactory(RestangularProvider) {
    RestangularProvider.setBaseUrl(baseURL);
}